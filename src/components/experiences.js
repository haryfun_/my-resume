import React from 'react';
import { Grid, Segment, Label, Step, Icon } from 'semantic-ui-react';
import { Parallax } from "react-parallax";

const Experiences = () => (
    <Parallax
      blur={{min: -15, max: 15} }
      bgImage={
        "https://scontent.fcgk6-1.fna.fbcdn.net/v/t1.0-9/17861576_1487190454636829_5283833842497472702_n.jpg?_nc_cat=0&oh=5cb65baccd47e8e0d08f0be6bb322c9c&oe=5BB405B7"
      }
      strength={300}
    >
    <Grid >
        <Grid.Row style={{paddingBottom:70, paddingTop:70}}>
            <Grid.Column width={2} />
            <Grid.Column width={12}  >
                <Segment  textAlign='center' raised>
                    <Label size='huge' as='h2' color='teal' attached='top' >
                    <Icon name='briefcase' />Experiences
                    </Label><br /><br />

                    <Step.Group widths={3} fluid>
                        <Step >
                            <Step.Content>
                                <Step.Title style={{ color: 'teal' }}>Coding Bootcamp </Step.Title>
                                <Step.Description style={{ fontStyle: 'italic', color: 'grey' }}>Mar 2018 - Jun 2018  </Step.Description>
                                <a href='https://refactory.id/' target='blank'><Step.Description>Refactory.id</Step.Description></a>
                            </Step.Content>
                        </Step>
                        <Step >
                            <Step.Content>
                                <Step.Title style={{ color: 'teal' }}>Computer and laptop technicians</Step.Title>
                                <Step.Description style={{ fontStyle: 'italic', color: 'grey' }}>Aug 2016 - Jan 2017 </Step.Description>
                                <Step.Description>ELM Computer</Step.Description>
                            </Step.Content>
                        </Step>

                    </Step.Group>
                    <Step.Group widths={3} fluid>
                        <Step >
                            <Step.Content>
                                <Step.Title style={{ color: 'teal' }}>Freelance Designer</Step.Title>
                                <Step.Description style={{ fontStyle: 'italic', color: 'grey' }}>Sep 2014 - Jul 2016 </Step.Description>
                                <Step.Description>
                                    Create interactive media using adobe flash.<br />
                                    Customizing CMS e.g wordpress, prestashop, lokomedia website.</Step.Description>
                            </Step.Content>
                        </Step>

                        <Step >
                            <Step.Content>
                                <Step.Title style={{ color: 'teal' }}>Staff Export - Import</Step.Title>
                                <Step.Description style={{ fontStyle: 'italic', color: 'grey' }}>Feb 2013 - Feb 2014 </Step.Description>
                                <Step.Description>PT Sanyo Jaya Components Indonesia</Step.Description>
                            </Step.Content>
                        </Step>
                        <Step >
                            <Step.Content>
                                <Step.Title style={{ color: 'teal' }}>Junior Web Designer</Step.Title>
                                <Step.Description style={{ fontStyle: 'italic', color: 'grey' }}>Feb 2011 - Jun 2011</Step.Description>
                                <Step.Description>PT CAKRAWALA GLOBAL INFORMATIKA</Step.Description>
                            </Step.Content>
                        </Step>
                    </Step.Group>
                </Segment>
            </Grid.Column>
            <Grid.Column width={2} />
        </Grid.Row>
    </Grid>
    </Parallax>
)

export default Experiences;