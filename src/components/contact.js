import React from 'react';
import { Grid, Icon, Button, Divider } from 'semantic-ui-react';

const Contact = () => (
    <Grid textAlign='center'>
        <Grid.Row style={{ padding: 0, backgroundColor: 'rgb(247, 247, 247)', textAlign: 'left' }}>
            <Button icon labelPosition='right' color='google plus' size='huge'>
                <h3>Contact Me</h3>
                <Icon name='right arrow' />
                <Grid.Column width={3} />
            </Button>
            <Grid.Column width={11}  >
                <br />
                <Button href='mailto:hary.vai@gmail.com?subject=hello' size='big' inverted color='red'>
                    <Icon name='envelope' /> Send Me email
                </Button>
                
                <Divider horizontal >Or</Divider>
                <a href='https://www.linkedin.com/in/haryana-28a85546/' target='blank'>
                    <Icon inverted color='black' name='linkedin' size='big' link />
                </a>
                <a href='https://github.com/haryanapnx' target='blank'>
                    <Icon inverted color='black' name='github' size='big' link />
                </a>
                <a href='https://www.facebook.com/hary.broadcaster' target='blank'>
                    <Icon inverted color='black' name='facebook' size='big' link />
                </a>
                <a href='https://plus.google.com/100532462697926841010' target='blank'>
                    <Icon inverted color='black' name='google plus' size='big' link />
                </a>
                <a href='https://www.instagram.com/haryfun_/' target='blank'>
                    <Icon inverted color='black' name='instagram' size='big' link />
                </a>
                <br /><br />
            </Grid.Column>
            <Grid.Column width={2} />
        </Grid.Row>
    </Grid>


)

export default Contact;