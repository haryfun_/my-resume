import React from 'react';
import {
    Container,
    List,
    Segment,
    Button,
    Icon
} from 'semantic-ui-react'
// import ScrollToTop from './onTop';
function scrollToTop() {
    window.scrollTo(0, 0);
}
const Footer = () => (
    <div>
        <Segment inverted style={{ margin: '2em 0em 0em', padding: '2.5em 0em' }} vertical>

            <Button floated='right' basic inverted color='red' onClick={scrollToTop}>
                <Icon name='arrow up' />Back to top
            </Button >
        </Segment>
        <Segment inverted vertical>
            <Container textAlign='center'>
                {/* <Divider inverted section /> */}
                <List horizontal inverted divided link >
                    <List.Item as='a' href='#' >
                        Develop with React js
                    </List.Item>
                    <List.Item as='a' href='#'>
                        Copyright 2018
                    </List.Item>
                </List>
            </Container>
        </Segment>
    </div>
)
export default Footer;