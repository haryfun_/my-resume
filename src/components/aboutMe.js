import React from "react";
import {
  Grid,
  Item,
  Icon,
  Container,
  Divider
} from "semantic-ui-react";
import styles from "../styles/style";
import me from "../asset/me.png";

const AboutMe = () => (
  <div>
    <Grid textAlign="left">
      <Grid.Column width={12} style={styles.styleAboutMe}>
        <Item.Group>
          <Item>
            <Item.Image size="large" src={me} />
            <Item.Content>
              <Container style={styles.descAboutMe}>
                <Divider hidden />
                Hi there,<br />my name is <b>Haryana</b>,<p>
                  an awesome and delightful guy, who has associate's degree from
                  Tri Mitra Karya Mandiri Politeknik in Informatics Engineering
                  and went to Refactory Programming School for further education
                  in programming.<br />
                  I'm someone, who has high interest in software development,
                  with expertise in some programming language, e.g{" "}
                  <strong>
                    HTML, CSS, React JS, and Javascript with Node JS
                  </strong>,
                  <br /> furthermore I'm confident with my skills in{" "}
                  <b>
                    Express JS framework, MySQL, Git workflow, wordpress, web
                    design and animations.
                  </b>
                </p>
                I have experiences working on several projects, like room
                reservations app and online financial investments, and some
                professional experiences in manufacturing company.
                <Divider hidden />
              </Container>
            </Item.Content>
          </Item>
        </Item.Group>
      </Grid.Column>
    </Grid>
    <Grid textAlign="center">
      <Grid.Column width={10} color="teal" style={{ marginBottom: "5em" }}>
        <span as="h3" style={{ color: "#fff" }}>
          Find Me on :{" "}
        </span>
        <a href="https://www.linkedin.com/in/haryana-28a85546/" target="blank">
          <Icon inverted color="grey" name="linkedin" size="big" link />
        </a>
        <a href="https://github.com/haryanapnx" target="blank">
          <Icon inverted color="grey" name="github" size="big" link />
        </a>
        <a href="https://www.facebook.com/hary.broadcaster" target="blank">
          <Icon inverted color="grey" name="facebook" size="big" link />
        </a>
        <a href="https://plus.google.com/100532462697926841010" target="blank">
          <Icon inverted color="grey" name="google plus" size="big" link />
        </a>
        <a href="https://www.instagram.com/haryfun_/" target="blank">
          <Icon inverted color="grey" name="instagram" size="big" link />
        </a>
      </Grid.Column>
    </Grid>
  </div>
);

export default AboutMe;
