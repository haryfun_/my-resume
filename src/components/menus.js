import React, { Component } from 'react';
import styles from '../styles/style';
import {
  Container,
  Menu,
  Visibility,
  Icon,
} from 'semantic-ui-react'

class Menus extends Component {
  state = {
    menuFixed: false,
    activeItem:'home'
  }

  stickTopMenu = () => this.setState({ menuFixed: true })

  unStickTopMenu = () => this.setState({ menuFixed: false })

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { menuFixed, activeItem } = this.state

    return (
        <Visibility
          onBottomPassed={this.stickTopMenu}
          onBottomVisible={this.unStickTopMenu}
          once={false}
          >
          <Menu
            color='pink'
            pointing secondary
            borderless
            fixed={menuFixed && "top"}
            style={menuFixed ? styles.fixedMenuStyle : styles.menuStyle}
            >
            <Container text textAlign='center' >

              <Menu.Item
              style={{color:'#fff'}}
              header
              href='#home'
              name='home'
              active={activeItem === 'home'}
              onClick={this.handleItemClick}>
                <Icon name='user circle' size='large' /> &nbsp;&nbsp;Who am I?
                </Menu.Item>

              <Menu.Item
                header
                href='#skillset'
                name='skillset'
                active={activeItem === 'skillset'}
                onClick={this.handleItemClick}>
                <Icon name='wrench' size='large' />&nbsp;&nbsp;Skillset
                </Menu.Item>

              <Menu.Item
                header
                href='#experiences'
                name='experience'
                active={activeItem === 'experience'}
                onClick={this.handleItemClick}>
                <Icon name='address book' size='large' />&nbsp;&nbsp;Experiences
                </Menu.Item>

              <Menu.Item
                header
                href='#contact'
                name='contact'
                active={activeItem === 'contact'}
                onClick={this.handleItemClick}>
                <Icon name='object ungroup outline' size='large' /> &nbsp;&nbsp;Contact
                </Menu.Item>

            </Container>
          </Menu>
        </Visibility>
    )
  }
}
export default Menus;