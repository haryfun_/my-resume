import React from 'react';
import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { Grid, Label } from 'semantic-ui-react';

const Skill = () => (
    <Grid style={{backgroundColor:'#f9f9f9', boxShadow: '0px 3px 5px rgba(0, 0, 0, 0.5)'}}>
        <Grid.Row >
            <Grid.Column width={3} />
            <Grid.Column width={10} textAlign='center' >
                <Grid doubling columns={5} >
                    <Grid.Column >
                        <CircularProgressbar
                            percentage={75}
                            styles={{
                                path: { stroke: `rgba(0, 128, 128, ${75 / 100})` },
                            }}
                        /><Label color='teal' pointing>React Js</Label>
                    </Grid.Column>
                    <Grid.Column>
                        <CircularProgressbar
                            percentage={70}
                            styles={{
                                path: { stroke: `rgba(234,48,51, ${70 / 100})` },
                            }}
                        /><Label color='red' pointing>Node Js</Label>
                    </Grid.Column>
                    <Grid.Column>
                        <CircularProgressbar
                            percentage={85}
                            styles={{
                                path: { stroke: `rgba(238,123,17, ${85 / 100})` },
                            }}
                        /><Label color='orange' size='big' pointing>CSS & HTML</Label>
                    </Grid.Column>
                    <Grid.Column>
                        <CircularProgressbar
                            percentage={80}
                            styles={{
                                path: { stroke: `rgb(234, 242, 14, ${80 / 100})` },
                            }}
                        /><Label color='yellow' pointing>JavaScript</Label>
                    </Grid.Column>
                    <Grid.Column>
                        <CircularProgressbar
                            percentage={95}
                            styles={{
                                path: { stroke: `rgba(62, 152, 199, ${95 / 100})` },
                            }}
                        /><Label color='black' pointing>Graphic Design</Label><br />
                        (adobe Creative & Corel Draw)
                    </Grid.Column>
                </Grid>
                <Grid doubling columns={5} >
                    <Grid.Column >
                        <CircularProgressbar
                            percentage={75}
                            styles={{
                                path: { stroke: `rgb(25, 64, 132, ${75 / 100})` },
                            }}
                        /><Label color='blue' pointing>mySql</Label>
                    </Grid.Column>
                    <Grid.Column>
                        <CircularProgressbar
                            percentage={70}
                            styles={{
                                path: { stroke: `rgba(0,0,0, ${70 / 100})` },
                            }}
                        /><Label color='black' pointing>Git</Label>
                    </Grid.Column>
                    <Grid.Column>
                        <CircularProgressbar
                            percentage={80}
                            styles={{
                                path: { stroke: `rgb(170, 170, 170, ${80 / 100})` },
                            }}
                        /><Label color='grey' size='big' pointing>Linux</Label>
                    </Grid.Column>
                    <Grid.Column>
                        <CircularProgressbar
                            percentage={60}
                            styles={{
                                path: { stroke: `rgb(114, 159, 255), ${60 / 100})` },
                            }}
                        /><Label style={{ backgroundColor: 'rgb(114, 159, 255)' }} pointing>Wordpress</Label>
                    </Grid.Column>
                    <Grid.Column>
                        <CircularProgressbar
                            percentage={100}
                            styles={{
                                path: { stroke: `rgb(70, 247, 0, ${100 / 100})` },
                            }}
                        /><Label style={{ backgroundColor: 'rgb(70, 247, 0' }} pointing>Sleep</Label>
                    </Grid.Column>
                </Grid>
            </Grid.Column>
            <Grid.Column width={3} />
        </Grid.Row>
    </Grid>


)

export default Skill;