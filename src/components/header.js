import React from "react";
import { Parallax } from "react-parallax";
import { Item, Button, Icon } from "semantic-ui-react";
import Typist from "react-typist";
import styles from "../styles/style";
import Menus from "./menus";
import hd from '../asset/h.jpg'

function printDocument() {
  window.print();
}
const Headers = () => (
  <div>
    <Parallax
      blur={{min: -15, max: 15}}
      bgImage={hd}
      strength={800}
    >
    <Menus />
      <div style={styles.typeWriter}>
        <Typist>
          <Item.Header as="h1"> Hello,</Item.Header>
          <Item.Meta as="h3">
            <Typist.Delay ms={500} />
            Welcome to my Profile :)
          </Item.Meta>
        </Typist>
        <Button inverted onClick={printDocument}>
          <Icon name="download" />Download CV
        </Button>
      </div>
    </Parallax>
  </div>
);
export default Headers;
