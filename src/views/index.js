import React, { Component } from 'react';
import Headers from '../components/header';
// import Menus from '../components/menus';
import Footer from '../components/footer';
import Skill from '../components/skill';
import AboutMe from '../components/aboutMe';
import Experiences from '../components/experiences';
import Contact from '../components/contact';
import { Divider, Icon } from "semantic-ui-react";
import styles from '../styles/style';

class Index extends Component {
  render() {
    return (
      <div id="divToPrint">
        <p style={styles.h2Style} id="home" />
        <Headers />
        <AboutMe />
        <p style={styles.h2Style} id="skillset" />
        <Divider horizontal>
          <h1>
            <Icon name='wrench' />Skillset
          </h1>
        </Divider><br />
        <Skill /><br />
        <p style={styles.h2Style} id="experiences"/>
        <Experiences />
        <Divider />
        <p style={styles.h2Style} id="contact"/>
        <Contact />
        <Divider />
        <Footer />
      </div>
    );
  }
}

export default Index;
