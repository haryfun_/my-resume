import Bg from '../asset/bg.png';
const styleAboutMe = {
  marginTop: '-270px',
  marginLeft: 'auto',
  marginRight: 'auto',
  backgroundColor: 'rgb(247, 247, 247)',
  backgroundImage: `url(${Bg})`,
  repeat: 'x',
  padding: '0 0 0 ',
  float: 'inherit',
  boxShadow: '0.2em 0.1em 0.2em 0.1em rgb(0,0,0, 0.5)',
  zIndex: 1,
};

const descAboutMe = {
  color: '#141414',
  fontSize: '1.5em',
  fontFamily: 'Rancho',
  paddingRight: 40,
  paddingLeft: 0,
  paddingTop: 0,
  paddingBottom: 0,
  textAlign: 'center',
  // textShadow: '0px 3px 5px rgba(0, 0, 0, 0.1)',
};

const typeWriter = {
  height: '500px',
  textAlign: 'center',
  marginTop: 50,
  textShadow: '0px 3px 5px rgba(0, 0, 0, 0.5)',
  color: '#fff',
};

const menuStyle = {
  color:'#fff',
  border: 'none',
  borderRadius: 0,
  boxShadow: 'none',
  marginBottom: '0em',
  marginTop: '0em',
  boxShadow: '0px 3px 5px rgba(0, 0, 0, 0.2)',
  transition: 'box-shadow 0.5s ease, padding 0.5s ease',
  transform: 'scale(1)',
  transition: '1s all ease'
}

const fixedMenuStyle = {
  boxShadow: '0px 3px 5px rgba(0, 0, 0, 0.2)',
  color:'#fff',
  backgroundColor: '#fff',
  transform: 'scale(1.1)',
  transition: '2s all ease',
  textAlign:'center'
}

const h2Style = {
  padding: 0,
  margin: 0,
  zIndex: -1
}

const styles = {
  styleAboutMe: styleAboutMe,
  typeWriter: typeWriter,
  descAboutMe: descAboutMe,
  menuStyle: menuStyle,
  fixedMenuStyle: fixedMenuStyle,
  h2Style: h2Style
}
export default styles;